<?php

namespace App\Controller;

use App\Document\Orders;
use Doctrine\ODM\MongoDB\DocumentManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class OrdersController extends AbstractController
{
    /**
     * @var DocumentManager
     */
    private $documentManager;
    private $listParameters;
    private $connection;
    private $db;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
        $this->listParameters = array("timestamp","terminal","kit_size","delivery_kit","know_how","trusted","order_level","storekeeper_level","vehicle","exclusive","type");
        $this->connection = new \MongoClient("mongodb://hackathonmongo:hackathon2018rappimongodb@mongo-hackathon.eastus2.cloudapp.azure.com:27017/orders");
        $this->db = $this->connection->selectDB('orders');
    }

    /**
     * @Route("/orders", name="orders_list")
     */
    public function listAction(Request $request)
    {  
        $params=$this->setParameters($request);
        if(empty($params))
             $params['timestamp']="2018-09-05 10:00:00";
        $orders = $this->db->selectCollection('orders');
        $ordersList = $orders->find($params);
        $response = [];
        if($ordersList->count() > 0){
            foreach ($ordersList as $order){
                $response['orders'][] = $order;
            }
        }
        return $response;
    }
    
    /**
     * returns parameters to filters
     * @param type $data
     * @return array
     */
    public function setParameters(Request $request){

        $params = array();
        
        foreach ($this->listParameters as $listParameters)
            $data[$listParameters]=$request->get($listParameters);
        
        if(isset($data['timestamp']))
        {
            $params['timestamp'] = $data['timestamp'];
        }
        if(isset($data['terminal']))
        {
            $params['toolkit.terminal'] = $data['terminal'];
        }
        if(isset($data['kit_size']))
        {
            $params['toolkit.kit_size'] = array('$lt'=>(int)$data['kit_size']);
        }
        if(isset($data['delivery_kit']))
        {
            $params['toolkit.delivery_kit'] = array('$lt'=>(int)$data['delivery_kit']);
        }
        if(isset($data['order_level']))
        {
            $params['toolkit.order_level'] = array('$lt'=>(int)$data['order_level']);
        }
        if(isset($data['storekeeper_level']))
        {
            $params['toolkit.storekeeper_level'] = array('$lt'=>(int)$data['storekeeper_level']);
        }
        if(isset($data['vehicle']))
        {
            $params['toolkit.vehicle'] = array('$lt'=>(int)$data['vehicle']);
        }
        if(isset($data['trusted']))
        {
            $params['toolkit.trusted'] = (boolean)$data['trusted'];
        }
         if(isset($data['exclusive']))
        {
            $params['toolkit.exclusive'] = (boolean)$data['exclusive'];
        }
        
        return $params;
    }
}