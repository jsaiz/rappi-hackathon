<?php

namespace App\Controller;

use App\Entity\StoreKeepers;
use App\Repository\StoreKeepersRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StorekeepersController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $listParameters;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->listParameters = array("timestamp","terminal","kit_size","delivery_kit","know_how","trusted","order_level","storekeeper_level","vehicle","exclusive","type");
        
    }

    /**
     * @Route("/storekeepers", name="storekeepers_list")
     * @Method("GET")
     * @param Request $request
     * @return mixed
     */
    public function listAction(Request $request)
    {
        $paramsFilter = $this->setParameters($request);
        $storeKeepers = $this->entityManager->getRepository(StoreKeepers::class)->getListRappidTendero($paramsFilter);
       
        $response = [];
        if(count($storeKeepers)){
            $response = [
                'storekeepers' => $storeKeepers
            ];
        }
        return $response;
    }
    
    /**
     * 
     * @param Request $request
     * @return array
     */
    public function setParameters(Request $request){   
        $params = array();
        
        foreach ($this->listParameters as $listParameters)
            $data[$listParameters]=$request->get($listParameters);     
        return $data;
    }
}