<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 15/09/18
 * Time: 02:02 PM
 */

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class StoreKeepersRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder(string $alias)
    {
        return $this->createQueryBuilder($alias);
    }
    
    /**
     * 
     * @param type $paramsFilter
     * @return type
     */
    public function getListRappidTendero($paramsFilter)
    {
        $params = "";
        if(isset($paramsFilter['timestamp']))
        {
            $params .= " AND timestamp = '{$paramsFilter['timestamp']}'";
        }
        if(isset($paramsFilter['vehicle']))
        {
             $params .= " AND (toolkit->>'vehicle')::int <= ".$paramsFilter['vehicle'];
        }
        if(isset($paramsFilter['trusted']))
        {
             $params .= " AND (toolkit->>'trusted')::boolean = ".$paramsFilter['trusted'];
        }
        if(isset($paramsFilter['kit_size']))
        {
             $params .= " AND (toolkit->>'kit_size')::int <= ".$paramsFilter['kit_size'];
        }
        if(isset($paramsFilter['know_how']))
        {
             $params .= " AND (toolkit->>'know_how')::int = ".$paramsFilter['know_how'];
        }
        if(isset($paramsFilter['delivery_kit']))
        {
             $params .= " AND (toolkit->>'delivery_kit')::int <= ".$paramsFilter['delivery_kit'];
        }
        if(isset($paramsFilter['order_level']))
        {
             $params .= " AND (toolkit->>'order_level')::int <= ".$paramsFilter['order_level'];
        }
        if(isset($paramsFilter['storekeeper_level']))
        {
             $params .= " AND (toolkit->>'storekeeper_level')::int <= ".$paramsFilter['storekeeper_level'];
        }
         if(isset($paramsFilter['exclusive']))
        {
             $params .= " AND (toolkit->>'exclusive')::boolean <= ".$paramsFilter['exclusive'];
        }
        
        $query = "
            SELECT  *
            FROM public.storekeepers
            WHERE id > 1 $params ;
        ";

        $preparedQuery = $this->_em->getConnection()->prepare($query);
        $preparedQuery->execute();
        return $preparedQuery->fetchAll();
    }

}