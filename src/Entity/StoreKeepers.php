<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class StoreKeepers
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\StoreKeepersRepository", readOnly=true)
 * @ORM\Table(name="storekeepers", schema="public")
 */
class StoreKeepers
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="storekeeper_id", type="integer")
     */
    private $storekeeperId;

    /**
     * @var double
     *
     * @ORM\Column(name="lat", type="float")
     */
    private $lat;

    /**
     * @var double
     *
     * @ORM\Column(name="lng", type="float")
     */
    private $lng;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var array
     *
     * @ORM\Column(name="toolkit", type="json")
     */
    private $toolkit;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStorekeeperId(): int
    {
        return $this->storekeeperId;
    }

    /**
     * @param int $storekeeperId
     */
    public function setStorekeeperId(int $storekeeperId): void
    {
        $this->storekeeperId = $storekeeperId;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp(\DateTime $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return array
     */
    public function getToolkit(): array
    {
        return $this->toolkit;
    }

    /**
     * @param array $toolkit
     */
    public function setToolkit(array $toolkit): void
    {
        $this->toolkit = $toolkit;
    }


}