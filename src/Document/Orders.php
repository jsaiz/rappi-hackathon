<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 15/09/18
 * Time: 02:37 PM
 */

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * Class Orders
 * @package App\Document
 *
 * @Document(collection="orders", repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @var float
     *
     * @Id(strategy="AUTO", type="float")
     */
    private $id;

    /**
     * @var float
     *
     * @Field(name="lat", type="float")
     */
    private $lat;

    /**
     * @var float
     *
     * @Field(name="lng", type="float")
     */
    private $lng;

    /**
     * @var string
     *
     * @Field(name="timestamp", type="string")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @Field(name="createdAt", type="string")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Field(name="type", type="string")
     */
    private $type;

    /**
     * @var
     *
     * @Field(name="toolkit", type="file")
     */
    private $toolkit;

    /**
     * @return float
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param float $id
     */
    public function setId(float $id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp(string $timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getToolkit()
    {
        return $this->toolkit;
    }

    /**
     * @param mixed $toolkit
     */
    public function setToolkit($toolkit)
    {
        $this->toolkit = $toolkit;
    }


}