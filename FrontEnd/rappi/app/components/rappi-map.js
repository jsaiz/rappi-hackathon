import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed, observer } from '@ember/object';
import { A } from '@ember/array';

export default Component.extend({
  store: service(),
  googleMapsApi: service(),
  showMap : false,
  storekeepers : null,
  heatMap : null,
  heatPointsArray : null,
  orders : null,
  cityPosition: null,
  trusted: true,
  vehicle: null,
  datetimeRappi: null,
  kitSize: null,
  knowHow: null,
  terminal: null,

  filteredOrders: computed(
    'selectedVehicle',
    'selectedKitsize',
    'selectedKnowHow',
    'selectedTerminal',
    'selectedOrderLevel',
    'selectedDeliveryKit',
    'selectedStorekeeperLevel',
    'orders', function () {
    return this.filterOrders();
  }),

  init(){
    let store = this.get('store');

    this.initFilters();

    this.set('storekeepers', A());
    this.set('orders', A());

    this.set('datetimeRappi', '2018-09-05 15:00:00')

    store.query('order', {timestamp: this.get('datetimeRappi')}).then(orders=>{
      this.set('orders', null);
      this.set('orders', orders);

      let first = orders.get('firstObject');
      this.set('cityPosition', null);
      this.set('cityPosition', {
        lat: first.get('lat'),
        lng: first.get('lng')
      });
      store.query('storekeeper', {timestamp: this.get('datetimeRappi')}).then(storekeeper=>{
        this.set('storekeepers', null);
        this.set('storekeepers', storekeeper);
        this.set('showMap', true);
      }).catch(failure => {
        console.log(failure)
      });
      }
    );

    this._super(...arguments);
  },

  filterFetchAll(){
    let store = this.get('store');
    this.set('showMap', false);
    store.query('order', {timestamp: this.get('datetimeRappi')}).then(orders=>{
        this.set('orders', null);
        this.set('orders', orders);

        let first = orders.get('firstObject');
        this.set('cityPosition', null);
        this.set('cityPosition', {
          lat: first.get('lat'),
          lng: first.get('lng')
        });
        store.query('storekeeper', {timestamp: this.get('datetimeRappi')}).then(storekeeper=>{
          this.set('storekeepers', null);
          this.set('storekeepers', storekeeper);
          this.set('showMap', true);
        }).catch(failure => {
          console.log(failure)
        });
      }
    );
  },

  filterOrders(){
    return this.orders.filter( filter => {
      let vehicle = this.filterOrderVehicle(filter);

      let kitSize = this.filterOrderKitSize(filter);

      let knowHow = this.filterOrderKnowHow(filter);
      let terminal = this.filterOrderTerminal(filter);
      let deliveryKit = this.filterOrderDeliveryKit(filter);
      let level = this.filterOrderLevel(filter);
      let storekeeperLevel = this.filterOrderStorekeeperLevel(filter);

      return vehicle && kitSize && knowHow && terminal && deliveryKit && level && storekeeperLevel
      }
    );
  },

  filterOrderVehicle(order) {
    if(typeof this.selectedVehicle === 'undefined' || null === this.selectedVehicle)
      return true;

    return order.get('toolkit').vehicle === this.selectedVehicle.id;
  },

  filterOrderKitSize(order) {
    if(typeof this.selectedKitsize === 'undefined' || null === this.selectedKitsize)
      return true;

    return order.get('toolkit')['kit_size'] === this.selectedKitsize.id;
  },

  filterOrderKnowHow(order) {
    if(typeof this.selectedKnowHow === 'undefined' || null === this.selectedKnowHow)
      return true;

    return order.get('toolkit')['know_how'] === this.selectedKnowHow.id;
  },

  filterOrderTerminal(order) {
    if(typeof this.selectedTerminal === 'undefined' || null === this.selectedTerminal)
      return true;

    return order.get('toolkit')['terminal'] === this.selectedTerminal.id;
  },

  filterOrderLevel(order) {
    if(typeof this.selectedOrderLevel === 'undefined' || null === this.selectedOrderLevel)
      return true;

    return order.get('toolkit')['order_level'] === this.selectedOrderLevel.id;
  },

  filterOrderDeliveryKit(order) {
    if(typeof this.selectedDeliveryKit === 'undefined' || null === this.selectedDeliveryKit)
      return true;

    return order.get('toolkit')['delivery_kit'] === this.selectedDeliveryKit.id;
  },

  filterOrderStorekeeperLevel(order) {
    if(typeof this.selectedStorekeeperLevel === 'undefined' || null === this.selectedStorekeeperLevel)
      return true;

    return order.get('toolkit')['storekeeper_level'] === this.selectedStorekeeperLevel.id;
  },

  /**
   * Inicialización de filtros
   */
  initFilters(){
    this.set('vehicle', [
      {
        id: 0,
        name : '0'
      },
      {
        id: 1,
        name : '1'
      },
      {
        id: 2,
        name : '2'
      }
    ]);

    this.set('kitSize', [
      {
        id: 0,
        name : '0'
      },
      {
        id: 1,
        name : '1'
      },
      {
        id: 2,
        name : '2'
      },
      {
        id: 3,
        name : '3'
      }
    ]);

    this.set('knowHow', [
      {
        id: 0,
        name : 'No'
      },
      {
        id: 1,
        name : 'Si'
      }
    ]);

    this.set('terminal', [
      {
        id: 0,
        name : 'No'
      },
      {
        id: 1,
        name : 'Si'
      }
    ]);

    this.set('orderLevel', [
      {
        id: 0,
        name : '0'
      },
      {
        id: 1,
        name : '1'
      },
      {
        id: 2,
        name : '2'
      },
      {
        id: 3,
        name : '3'
      },
      {
        id: 4,
        name : '4'
      },
      {
        id: 5,
        name : '5'
      },
      {
        id: 6,
        name : '6'
      },
      {
        id: 7,
        name : '7'
      },
      {
        id: 8,
        name : '8'
      }
    ]);

    this.set('deliveryKit', [
      {
        id: 0,
        name : '0'
      },
      {
        id: 1,
        name : '1'
      }
    ]);

    this.set('cityPosition', {
      lat: '51.508530',
      lng: '-0.076132'
    });

    this.set('storekeeperLevel', [
      {
        id: 0,
        name : '0'
      },
      {
        id: 1,
        name : '1'
      },
      {
        id: 2,
        name : '2'
      },
      {
        id: 3,
        name : '3'
      },
      {
        id: 4,
        name : '4'
      },
      {
        id: 5,
        name : '5'
      },
      {
        id: 6,
        name : '6'
      },
      {
        id: 7,
        name : '7'
      },
      {
        id: 8,
        name : '8'
      }
    ]);
  },

  heatPoints(storekeepers){
    let heatmapData = [];
    storekeepers.forEach((storekeeper) => {
        let latLng = new google.maps.LatLng(storekeeper.get('lat'), storekeeper.get('lng'));
        heatmapData.push(latLng);
      }
    );

    return new google.maps.MVCArray(heatmapData);
  },

  filterStorekeepersByOrder(order){
    let filteredStorekeepers = this.get('storekeepers').filter( storekeeper => {
        let distance = this.calculateDistance(order, storekeeper);
        let terminal = storekeeper.get('computedToolkit')['terminal'] == order.get('toolkit')['terminal'];
        let knowHow = storekeeper.get('computedToolkit')['know_how'] == order.get('toolkit')['know_how'];
        let trusted = storekeeper.get('computedToolkit')['trusted'] == order.get('toolkit')['trusted'];
        return distance <= 0.02 && terminal && knowHow && trusted;
      }
    );

    this.get('heatPointsArray').clear();
    
    filteredStorekeepers.forEach(keeper => {
      let latLng = new google.maps.LatLng(keeper.get('lat'), keeper.get('lng'));
      this.get('heatPointsArray').push(latLng);
    });
  },

  calculateDistance(order, storekeeper){
    let lat = order.get('lat') - storekeeper.get('lat');
    let lng = order.get('lng') - storekeeper.get('lng');

    return Math.sqrt(
      Math.pow(lat, 2) + Math.pow(lng, 2)
    );
  },

  actions: {
    onLoad({ map }) {
      this.set('heatPointsArray', this.heatPoints(this.get('storekeepers')));
      let heatMap = new google.maps.visualization.HeatmapLayer({
        data: this.get('heatPointsArray'),
        map: map
      });

      this.set('heatMap', heatMap);
    },

    onClick(order){
      this.filterStorekeepersByOrder(order);
    },

    onKeyPress(){
      this.filterFetchAll()
    }
  }
});
