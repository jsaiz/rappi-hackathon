import DS from 'ember-data';

export default DS.Model.extend({
  lat: DS.attr(),
  lng: DS.attr(),
  timestamp: DS.attr(),
  createdAt: DS.attr(),
  type: DS.attr(),
  toolkit: DS.attr()
});
