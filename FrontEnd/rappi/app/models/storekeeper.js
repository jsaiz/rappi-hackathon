import DS from 'ember-data';
import { computed, observer } from '@ember/object';
import $ from 'jquery';

export default DS.Model.extend({
  storekeeper_id: DS.attr('string'),
  lat: DS.attr(),
  lng: DS.attr(),
  timestamp: DS.attr(),
  toolkit:DS.attr(),
  computedToolkit: computed('toolkit', function () {
    return $.parseJSON(this.get('toolkit'));
  })
});
